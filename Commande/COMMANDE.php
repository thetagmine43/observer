<? php 

espace de noms  RefactoringGuru \Command \Conceptual ; 


// Command(visuel) déclare une méthode pour exécuter une commande. 
interface  Visuel 
{ 
    public  function  execute ( ) :  void ; 
} 

 
// Certaines commandes peuvent implémenter des opérations simples par elles-mêmes. 
class  SimpleBouton  implémente  Visuel 
{ 
    private  $payload ; 

    public  function  __construct ( string  $payload ) 
    { 
        $this -> payload  =  $payload ; 
    } 

    public  function  execute ( ) :  void 
    { 
        echo  " SimpleBouton : Vous voyez, je peux faire des choses simples comme imprimer ("  .  $this -> payload  .  " )\n" ; 
    } 
} 


// Cependant, certaines commandes peuvent déléguer des opérations plus complexes à d'autres objets, 
class  ComplexVisuel  implémente  Visuel 
{ 
    
    // @var Receiver 
    private  $receiver ; 

    // Données de contexte, requises pour lancer les méthodes du récepteur. 
    privé  $a ; 
    privé  $b ; 

    
    // Les commandes complexes peuvent accepter un ou plusieurs objets récepteurs
    public  function  __construct ( Receiver  $receiver ,  string  $a ,  string  $b ) 
    { 
        $this -> receiver  =  $receiver ; 
        $ce -> a  =  $a ; 
        $ce -> b  =  $b ; 
    } 

    
    // Les commandes peuvent déléguer à n'importe quelle méthode d'un récepteur. 
    public  function  execute ( ) :  void 
    { 
        echo  " VisuelComplexe : les choses complexes doivent être effectuées par un objet récepteur.\n"  ; 
        $this -> receiver -> doSomething ( $this -> a ) ; 
        $this -> receiver -> doSomethingElse ( $this -> b ) ; 
    } 
} 

// Les classes Receiver contiennent une logique métier importante. 
//   -Ils savent effectuer toutes sortes d'opérations, liées à l'exécution d'une requête. 
//   -En fait, n'importe quelle classe peut servir de récepteur. 
class  Receiver 
{ 
    public  function  doSomething ( string  $a ) :  void 
    { 
        echo  " Receiver: Working on ("  .  $a  .  " .)\n" ; 
    } 

    public  function  doSomethingElse ( string  $b ) :  void 
    { 
        echo  " Récepteur : travaille également sur ("  .  $b  .  " .)\n" ; 
    } 
} 

// L'Invoker est associé à une ou plusieurs commandes. Il envoie une requête à 
// la commande. 
class  Invoker 
{ 
    // @var Command 
    private  $onStart ; 

    // @var Command 
    private  $onFinish ; 
   
    // Initialiser les commandes. 
    public  function  setOnStart ( Command  $command ) :  void 
    { 
        $this -> onStart  =  $command ; 
    } 
    public  function  setOnFinish ( Command  $command ) :  void 
    { 
        $this -> onFinish  =  $command ; 
    } 

    
    // L'Invoker ne dépend pas de classes de commandes ou de récepteurs concrets. Les 
    // L'appelant transmet une requête à un récepteur indirectement, en exécutant une 
    // commande. 
    public  function  doSomethingImportant ( ) :  void 
    { 
        echo  " Invoker : Quelqu'un veut-il que quelque chose soit fait avant que je commence ?\n" ; 
        if  ( $this -> onStart  instanceof  Command )  { 
            $this -> onStart -> execute ( ) ; 
        } 

        echo  " Invoker : ...faire quelque chose de vraiment important...\n" ; 

        echo  " Appeleur : Est-ce que quelqu'un veut que quelque chose soit fait après que j'ai fini ?\n" ; 
        if  ( $this -> onFinish  instanceof  Command )  { 
            $this -> onFinish -> execute ( ) ; 
        } 
    } 
} 

// Le code client peut paramétrer un invocateur avec n'importe quelle commande. 
$invoker  =  new  Invoker ( ) ; 
$invoker -> setOnStart ( new  SimpleCommand bonjour ( " Dites ! ) ) ; 
$receiver  =  nouveau  récepteur ( ) ; 
$invoker -> setOnFinish ( new  ComplexCommand ( $receiver ,  " Send email" ,  " Save report" ) ) ; 

$invoker -> doSomethingImportant ( ) ; 