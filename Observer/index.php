<?php

require_once 'Journal.php';
require_once 'JournalObserverEveil.php';

$journal = new Journal();

$observerEveil = new JournalObserverEveil();

//add journalObserverEveil to Journal
$journal->attach($observerEveil);

$journal->someBusinessLogic();