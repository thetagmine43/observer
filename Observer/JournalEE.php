<?php

// Averti lorsque il y a un nouvel évenement
class Journal implements SplSubject
{
    //Etat de l'objet déclaré ci dessus
    public $state;

    // SplObjectStorage = Liste des abonnés
    private $ouestFrance;

    public function __construct()
    {
        $this->ouestFrance = new SplObjectStorage();
    }

    //les Méthodes
    public function attach(SplObserver $observer): void
    {
        //Objet : Attaché un observateur(journal)
        echo "Journal: Attached an observer.\n";
        $this->ouestFrance->attach($observer);
    }

    public function detach(SplObserver $observer): void
    {
        $this->ouestFrance->detach($observer);
        echo "Journal: Detached an observer.\n";
        // Objet : Enlève un observateur(journal)
    }

    //logique métier, déclenche une méthode de notification pour chaque évenements
    public function someBusinessLogic(): void
    {
        echo "\nJournal: I'm doing something important.\n";
        $this->state = random_int(0, 10);

        echo "Journal: My state has just changed to: {$this->state}\n";
        $this->notify();
    }

    //mise à jour pour chaque abonné
    public function notify(): void
    {
        echo "Journal: Notifying observers...\n";
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }
}

//Les Observateurs(journal) réagissent aux mises à jour du Sujet(ouestFrance) auquel ils sont attachés. 
class JournalObserverA implements \SplObserver
{
    public function update(\SplJournal $journal): void
    {
        if ($journal->state < 3) {
            echo "JournalObserverA: A réagi à l'événement.\n";
        }
    }
}

class JournalObserverB implements \SplObserver
{
    public function update(\SplJournal $journal): void
    {
        if ($journal->state == 0 || $journal->state >= 2) {
            echo "JournalObserverB: Reacted to the event.\n";
        }
    }
}

/**
 * The client code.
 */

$journal = new Journal();

$o1 = new JournalObserverrA();
$journal->attach($o1);

$o2 = new JournalObserverB();
$journal->attach($o2);

$journal->someBusinessLogic();
$journal->someBusinessLogic();

$journal->detach($o2);

$journal->someBusinessLogic();