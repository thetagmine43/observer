<?php

/**
 * The Subject owns some important state and notifies observers when the state
 * changes.
 */
class Journal implements SplSubject
{
    /**
     * @var int For the sake of simplicity, the Journal's state, essential to
     * all subscribers, is stored in this variable.
     */
    public $state;

    /**
     * @var SplObjectStorage List of subscribers. In real life, the list of
     * subscribers can be stored more comprehensively (categorized by event
     * type, etc.).
     */
    private $observers;

    public function __construct()
    {
        $this->observers = new SplObjectStorage();
    }

    /**
     * The subscription management methods.
     */
    public function attach(SplObserver $observer): void
    {
        echo "Journal: Attached an observer.\n";
        $this->observers->attach($observer);
    }

    public function detach(SplObserver $observer): void
    {
        $this->observers->detach($observer);
        echo "Journal: Detached an observer.\n";
    }

    /**
     * Usually, the subscription logic is only a fraction of what a Journal can
     * really do. Subjects commonly hold some important business logic, that
     * triggers a notification method whenever something important is about to
     * happen (or after it).
     */
    public function someBusinessLogic(): void
    {
        echo "\nJournal: I'm doing something important.\n";
        $this->state = random_int(0, 10);

        echo "Journal: My state has just changed to: {$this->state}\n";
        $this->notify();
    }

    /**
     * Trigger an update in each subscriber.
     */
    public function notify(): void
    {
        echo "Journal: Notifying observers...\n";
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }
}