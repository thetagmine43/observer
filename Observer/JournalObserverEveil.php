<?php

class JournalObserverEveil implements SplObserver
{
    /**Concrete Observers react to the updates issued by the Subject they had been
    attached to.
     */
    public function update(SplSubject $subject)
    {
        if ($subject->state < 3)
        {
            echo "Journal Eveil : nouvelle information \n";
        }
    }
}