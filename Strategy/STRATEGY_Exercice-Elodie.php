<?php


// Le contexte définit l'interface d'intérêt pour les clients.
class Itinéraire//contexte
{
    // le Context accepte une stratégie
    private $chemin;//strategy

    // le Context accepte une stratégie via le constructeur
    public function __construct(Chemin $chemin)//constructeur
    {
        $this->chemin = $chemin;
    }

    // le Context permet de remplacer un objet Strategy à l'exécution
    public function setChemin(Chemin $chemin)
    {
        $this->chemin = $chemin;
    }

    // Le contexte délègue une partie du travail à l'objet Strategy au lieu de 
    // implémenter plusieurs versions de l'algorithme par lui-même.
    public function doSomeBusinessLogic(): void
    {
        // ...

        echo "Itinéraire : tri des données à l'aide de la stratégie (je ne sais pas comment cela va le faire)\n";
        $result = $this->chemin->doAlgorithm(["a", "b", "c", "d", "e"]);
        echo implode(",", $result) . "\n";

        // ...
    }
}

// L'interface Strategy déclare des opérations communes à toutes les versions prises en charge 
// de certains algorithmes. 
 
interface Chemin
{
    public function doAlgorithm(array $data): array;
}
 
// Strategies implémente l'algorithme en suivant la base Strategy 
class ConcreteCheminAPied implements Chemin
{
    public function doAlgorithm(array $data): array
    {
        sort($data);

        return $data;
    }
}

class ConcreteCheminVelo implements Chemin
{
    public function doAlgorithm(array $data): array
    {
        rsort($data);

        return $data;
    }
}

// Le code client choisit une stratégie concrète et la passe au contexte.
 
$itinéraire = new Context(new ConcreteCheminAPied());
echo "Client : la stratégie est définie sur un tri normal.\n";
$itinéraire->doSomeBusinessLogic();

echo "\n";

echo "Client : la stratégie est définie pour le tri inversé.\n";
$itinéraire->setChemin(new ConcreteCheminVelo());
$itinéraire->doSomeBusinessLogic();
